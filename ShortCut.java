    // 添加快捷方式：

     	private void addShortCutToDestop() {

		String className = getPackageName() + "." + getLocalClassName();
		Intent intent = new Intent(Intent.ACTION_MAIN);
		intent.setClassName(MainActivity.this, className);
		intent.addCategory(Intent.CATEGORY_LAUNCHER);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		intent.addFlags(Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);

		String appName = getString(R.string.app_name);
		Log.d(TAG, " addShortCutToDestop--->  className : " + className);
		Log.d(TAG, " addShortCutToDestop--->  appName : " + appName);
		Intent shortCut = new Intent();
		shortCut.putExtra(Intent.EXTRA_SHORTCUT_INTENT, intent);
		shortCut.putExtra(Intent.EXTRA_SHORTCUT_NAME, appName);
		Intent.ShortcutIconResource icon = Intent.ShortcutIconResource.fromContext(this, R.drawable.ic_action_search);
		shortCut.putExtra(Intent.EXTRA_SHORTCUT_ICON_RESOURCE, icon);
		shortCut.putExtra("duplicate", false);
		shortCut.setAction("com.android.launcher.action.INSTALL_SHORTCUT");
		sendBroadcast(shortCut);

	}

         // 检查是否已经创建了快捷键

    	private boolean isHasShortCut() {

		boolean isHasShortCut = false;

		ContentResolver cp = getContentResolver();

		String authority = "com.android.launcher.settings";

		Uri content_uri = Uri.parse("content://" + authority + "/favorites?notify=true");

		String appName = getString(R.string.app_name);

		Cursor c = cp.query(content_uri, new String[] { "title", "iconResource" }, "title=?", new String[] { appName }, null);

		if (c != null && c.getCount() > 0) {
			Log.d("TEST", " --- >1 isHasShortCut : " + isHasShortCut);
			isHasShortCut = true;
		}

		Log.d("TEST", " isHasShortCut--- > c : " + c);
		Log.d("TEST", " isHasShortCut--- > isHasShortCut : " + isHasShortCut);
		Log.d("TEST", " isHasShortCut --- > appName : " + appName);
		return isHasShortCut;

	}

             // 删除快捷键
	private void delShortCut() {

		String className = getPackageName() + "." + getLocalClassName();
		Intent intent = new Intent(Intent.ACTION_MAIN);
		intent.setClassName(MainActivity.this, className);
		intent.addCategory(Intent.CATEGORY_LAUNCHER);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		intent.addFlags(Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);

		String appName = getString(R.string.app_name);
		Log.d(TAG, " addShortCutToDestop--->  className : " + className);
		Log.d(TAG, " addShortCutToDestop--->  appName : " + appName);
		Intent shortCut = new Intent();
		shortCut.putExtra(Intent.EXTRA_SHORTCUT_INTENT, intent);
		shortCut.putExtra(Intent.EXTRA_SHORTCUT_NAME, appName);
		Intent.ShortcutIconResource icon = Intent.ShortcutIconResource.fromContext(this, R.drawable.ic_action_search);
		shortCut.putExtra(Intent.EXTRA_SHORTCUT_ICON_RESOURCE, icon);
		shortCut.putExtra("duplicate", false);
		shortCut.setAction("com.android.launcher.action.UNINSTALL_SHORTCUT");

		sendBroadcast(intetn);

	}

        注意：  

              添加和删除快捷方式的intent内容要保证完全一样，只是action不一样，否则删除不生效


    
